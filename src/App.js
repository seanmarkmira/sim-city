import Home from "./pages/Home";
import AppNavBar from "./components/AppNavBar";
import Contact from "./pages/Contact";
import Checkout from "./pages/Checkout";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Routes,
} from "react-router-dom";

function App() {
  return (
    <>
      <AppNavBar />
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/contact" element={<Contact />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
