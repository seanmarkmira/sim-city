const exam_data = [
	{
		id: 1,
		product_code: "ult_small",
		product_name: "Unlimited 1GB",
		image_url:
			"https://cdn.shopify.com/s/files/1/2954/7774/products/GoogleShoppingSIMkit1_ad5e79ba-a3ab-4bf8-8949-54e76bc874e0.png?v=1564710358",
		price: 24.9,
	},
	{
		id: 2,
		product_code: "ult_medium",
		product_name: "Unlimited 2GB",
		image_url:
			"https://cdnp1.stackassets.com/b821b37b5672facfb493c1005b43dbfaaa1d3c4e/store/opt/596/447/8437fde5a5f28fd4de25797b9be5acb61408d22bdf48af216a7ab8107a58/product_313548_product_shots3.jpg",
		price: 29.9,
	},
	{
		id: 3,
		product_code: "ult_large",
		product_name: "Unlimited 5GB",
		image_url: "https://cf.shopee.ph/file/0f4e25f3bbac783632856c7feefe4b9a",
		price: 44.9,
	},
	{
		id: 4,
		product_code: "1gb",
		product_name: "1 GB Data-pack",
		image_url:
			"https://cdn.cheapoguides.com/wp-content/uploads/sites/2/2019/08/cheapo-sim-card_rb-770x385.jpg",
		price: 9.9,
	},
];

export default exam_data;
