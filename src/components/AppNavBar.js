import React, { useEffect, useState } from "react";
import {
  Navbar,
  Container,
  NavDropdown,
  Nav,
  Button,
  Modal,
} from "react-bootstrap";

const AppNavBar = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  //   const Test = () => {
  //     console.log("turn the modal open");
  //
  //     return (
  //       <>
  //
  //       </>
  //     );
  //   };

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Some info~</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          This is a personal project based on an exam given by a recruiter. I
          really had fun making this one. It's a great exercise to review react
          concepts and array/object manipulation. Links are provided below for
          your consumption.
          <ul>
            <li>Exam details</li>
            <li>Source code of the project</li>
            <li>Important concepts learned</li>
          </ul>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Navbar bg="light" expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/">Shopping Cart Exercise</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link onClick={handleShow}>readMe</Nav.Link>
              <Nav.Link href="/checkout">Checkout Cart</Nav.Link>
              <Nav.Link href="/contact">Contact Me</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default AppNavBar;
