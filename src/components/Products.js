import React, { useEffect, useState } from "react";
import { Card, Button, Container, Row, Col, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import exam_data from "./product_data";

/*
  Next goals:
    1. To be able to save in local storage/browser storage the current cart upon checkout (done)
    2. If checkout, it will lead to the checkout cart page (done)
    3. The checkout cart will have the promotion applied
    4. Must be able to minus a specific product in the current cart
*/

/*
  List of bugs:
    1. Not able to see the checkout button after adding 1 product
      answer: Create an internal component and then use the setter to set show to True (Done - my logic does not support the structure of the new object)
    2. From checkout to home, the useState variable does not catch our localstorage ( done - this is done)
    3. Check out must not show if there is no product
    4. Clear button must clear all of the product
*/
const Products = () => {
  const navigate = useNavigate();
  const localCheckout =
    JSON.parse(localStorage.getItem("checkoutProducts")) || [];
  const [cartProduct, setCartProduct] = useState(localCheckout);
  // const [cartProduct, setCartProduct] = useState([]);
  const [showCheckoutBut, setShowCheckoutBut] = useState(false);
  //To be triggered for every press of 'add to cart'
  const addToCart = (name, code, price) => {
    var objectHolder = {
      name: name,
      code: code,
      price: price,
    };
    setCartProduct(() => {
      return [...cartProduct, objectHolder];
    });
  };

  //This is the products card that we have
  const ProductsCard = () => {
    const cards = exam_data.map((value, index) => {
      return (
        <Col key={value.id}>
          <Card style={{ width: "18rem" }}>
            <Card.Img
              variant="top"
              src={value.image_url}
              className="product_image"
            />
            <Card.Body>
              <Card.Title>{value.product_name}</Card.Title>
              <Card.Text>Code:{value.product_code}</Card.Text>
              <Button
                variant="primary"
                onClick={() => {
                  addToCart(
                    value.product_name,
                    value.product_code,
                    value.price
                  );
                }}
              >
                Add to Cart
              </Button>
            </Card.Body>
          </Card>
        </Col>
      );
    });

    return cards;
  };

  const CurrentCart = () => {
    console.log("This is from the localstorage getItem");
    console.log(JSON.parse(localStorage.getItem("checkoutProducts")));
    console.log(cartProduct);
    // console.log(cartProduct.length);
    console.log("currentCart called");

    var formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      // These options are needed to round to whole numbers if that's what you want.
      //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
      //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
    //TESTING HERE
    window.currentProducts = [];
    //TESTING HERE
    //To get the unique products coming in

    const uniqueProduct = Array.from(
      new Set(cartProduct.map((item) => item.code))
    );

    //To create an object of product, price, and quantity to be presented in table
    uniqueProduct.forEach((value, index) => {
      var count = 0;
      //To get the price of the product
      var priceProduct = cartProduct.find((product) => {
        return value == product.code;
      });

      //To count how many instance of product AND to get that productName
      for (const property in cartProduct) {
        if (cartProduct[property].code == value) {
          console.log("There is a hit, it must be 4");
          var productName = cartProduct[property].name;
          count += 1;
        }
      }
      //To create that obj placeholder and pushing that object to array
      const obj = {
        name: productName,
        code: value,
        quantity: count,
        price: priceProduct.price,
      };
      window.currentProducts.push(obj);
    });

    //To finally show the cart
    var showCart = window.currentProducts.map((value, index) => {
      return (
        <tr key={`${Math.floor(Math.random() * 3)}${index}`}>
          <td>{value.name}</td>
          <td>{value.code}</td>
          <td>{value.quantity}</td>
          <td>{formatter.format(value.price * value.quantity)}</td>
        </tr>
      );
    });

    //using localstorage if there are items
    localStorage.setItem(
      "summationProducts",
      JSON.stringify(window.currentProducts)
    );
    localStorage.setItem("checkoutProducts", JSON.stringify(cartProduct));
    return showCart;
  };

  //For every update of the cartProduct, it will be refreshed and CurrentCart will be called
  useEffect(() => {
    CurrentCart();
  }, [cartProduct]);

  return (
    <>
      <Container className="product_box">
        <Row md={2} lg={4} sm={1}>
          <ProductsCard />
        </Row>
      </Container>
      <h1 align="center">Current Cart</h1>
      <Table>
        <thead>
          <tr>
            <th>Product</th>
            <th>Item Code</th>
            <th>Quantity</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          <CurrentCart />
        </tbody>
      </Table>
      {/* This must only show when there is product */}
      {JSON.parse(localStorage.getItem("checkoutProducts")) != null && (
        <Button
          variant="primary"
          className="align-self-center mx-auto"
          onClick={() => {
            navigate("/checkout");
          }}
        >
          Checkout
        </Button>
      )}
      <Button
        variant="primary"
        className="align-self-center mx-auto"
        onClick={() => {
          localStorage.removeItem("checkoutProducts");
        }}
      >
        CLEAR
      </Button>
    </>
  );
};

export default Products;
