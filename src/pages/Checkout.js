import { Button, Row, Col, Table } from "react-bootstrap";
import React, { useEffect, useContext } from "react";

//There is a problem with the price of the object
const Checkout = () => {
	var localCheckout = JSON.parse(localStorage.getItem("summationProducts"));
	var formatter = new Intl.NumberFormat("en-US", {
		style: "currency",
		currency: "USD",
		// These options are needed to round to whole numbers if that's what you want.
		//minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
		//maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
	});
	localCheckout.map((value, index) => {
		Object.assign(localCheckout[index], {
			promotion: "No promotion applied",
			totalPrice: 0,
		});
	});

	const ApplyPromotion = () => {
		var promotionApplied = false;
		localCheckout.map((value, index) => {
			//Make promotion for first offering
			if (
				value.code == "ult_small" &&
				value.quantity >= 3 &&
				promotionApplied == false
			) {
				var threeCount = Math.floor(value.quantity / 3) * 2;
				var threeRemainder = value.quantity % 3;
				var totalCount = threeCount + threeRemainder;
				var finalPrice_FirstPromotion = totalCount * 24.9;
				promotionApplied = true;
				value.promotion = `Promotion applied for Unlimited 1GB: ${formatter.format(
					finalPrice_FirstPromotion
				)}`;
				value.totalPrice = finalPrice_FirstPromotion;
				//Make promotion for second offering
			} else if (
				value.code == "ult_large" &&
				value.quantity >= 3 &&
				promotionApplied == false
			) {
				value.promotion = `More than 3 promotion applied! $39.90 ea!`;
				value.price = 39.9;
				promotionApplied = true;
				//Make promotion for third offering
			} else if (
				value.code == "ult_medium" &&
				promotionApplied == false
			) {
				const find1GB = localCheckout.find(({ code }) => code == "1gb");
				if (find1GB == null) {
					localCheckout.push({
						name: "1 GB Data-pack",
						code: "1gb",
						quantity: value.quantity,
						price: 0,
						totalPrice: 0,
						promotion: "Get 1 free for every 2GB unlimtied!",
					});
				} else {
					var promotionQuantity = value.quantity + value.quantity;
					find1GB.promotion = `Total quantity to ${promotionQuantity}. Promo from Get 1 free for every 2 GB ULT!`;
				}
				promotionApplied = true;
			}
		});

		if (localCheckout.length != 0) {
			var grandTotal = 0;
			var checkoutWithPromotion = localCheckout.map((value, index) => {
				if (value.totalPrice == 0) {
					value.totalPrice = value.quantity * value.price;
				}

				grandTotal = value.totalPrice + grandTotal;

				if (index == localCheckout.length - 1) {
					return (
						<tbody key={`${Math.floor(Math.random() * 3)}${index}`}>
							<tr>
								<td>{value.code}</td>
								<td>{value.name}</td>
								<td>{value.quantity}</td>
								<td>{formatter.format(value.price)}</td>
								<td>{value.promotion}</td>
								<td>{formatter.format(value.totalPrice)}</td>
							</tr>
							<tr>
								<td colSpan={5}>Grand Total</td>
								<td>{formatter.format(grandTotal)}</td>
							</tr>
						</tbody>
					);
				} else {
					return (
						<tbody key={`${Math.floor(Math.random() * 3)}${index}`}>
							<tr>
								<td>{value.code}</td>
								<td>{value.name}</td>
								<td>{value.quantity}</td>
								<td>{formatter.format(value.price)}</td>
								<td>{value.promotion}</td>
								<td>{formatter.format(value.totalPrice)}</td>
							</tr>
						</tbody>
					);
				}
			});
		} else {
			return (
				<tbody>
					<tr>
						<td colSpan={5}>
							{"There are no products in your cart!"}
						</td>
					</tr>
				</tbody>
			);
		}
		return checkoutWithPromotion;
	};

	// const CheckoutTable = () => {
	// 	console.log(" CALLLED CHECKOUT TABLE");
	// 	var formatter = new Intl.NumberFormat("en-US", {
	// 		style: "currency",
	// 		currency: "USD",
	// 		// These options are needed to round to whole numbers if that's what you want.
	// 		//minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
	// 		//maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
	// 	});
	// 	if (localCheckout.length != 0) {
	// 		var CheckoutTable = localCheckout.map((value, index) => {
	// 			return (
	// 				<tbody key={`${Math.floor(Math.random() * 3)}${index}`}>
	// 					<tr>
	// 						<td>{value.code}</td>
	// 						<td>{value.name}</td>
	// 						<td>{value.quantity}</td>
	// 						<td>
	// 							{formatter.format(value.price * value.quantity)}
	// 						</td>
	// 						{/* <td>{terneary poperator, if promotion exist display promotion else promotion already applied at what product}</td> */}
	// 						<td>{value.promotion}</td>
	// 					</tr>
	// 				</tbody>
	// 			);
	// 		});
	// 	} else {
	// 		return (
	// 			<tbody>
	// 				<tr>
	// 					<td colSpan={5}>
	// 						{"There are no products in your cart!"}
	// 					</td>
	// 				</tr>
	// 			</tbody>
	// 		);
	// 	}
	// 	return CheckoutTable;
	// };
	useEffect(() => {
		ApplyPromotion();
	}, []);

	return (
		<>
			<Table striped bordered hover variant="dark">
				<thead>
					<tr>
						<th>Product Code</th>
						<th>Product Name</th>
						<th>Quantity</th>
						<th>Price Ea</th>
						<th>Promotion Applied</th>
						<th>Total Price</th>
					</tr>
				</thead>
				<ApplyPromotion />
			</Table>
		</>
	);
};

export default Checkout;
